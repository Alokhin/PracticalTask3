﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PracticalTask3.Models;

namespace PracticalTask3.Serializers
{
    public class JsonCustomSerializer : CustomSerializer
    {
        public override List<BankClient> ListBankClientDeserialize(StreamReader reader)
        {
            return JsonConvert.DeserializeObject<List<BankClient>>(reader.ReadToEnd());
        }

        public override void ReportSerialize(StreamWriter writer, Report report)
        {
            writer.WriteLine(JsonConvert.SerializeObject(report,
                Formatting.Indented,
                new IsoDateTimeConverter() {DateTimeFormat = "dd.MM.yyyy"}
                )
            );
        }
    }
}