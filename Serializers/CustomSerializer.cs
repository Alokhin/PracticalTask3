﻿using System.Collections.Generic;
using System.IO;
using PracticalTask3.Models;

namespace PracticalTask3.Serializers
{
    public abstract class CustomSerializer
    {
        public abstract List<BankClient> ListBankClientDeserialize(StreamReader reader);
        public abstract void ReportSerialize(StreamWriter writer, Report report);
        
        public List<BankClient> ListBankClientDeserialize(string fileDir)
        {
            using (var reader = new StreamReader(fileDir))
            {
                return ListBankClientDeserialize(reader);
            }
        }
        
        public void ReportSerialize(string fileDir, Report report)
        {
            using (var writer = new StreamWriter(fileDir))
            {
                ReportSerialize(writer, report);
            }
        }
    }
}