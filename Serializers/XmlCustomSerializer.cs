﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using PracticalTask3.Models;

namespace PracticalTask3.Serializers
{
    public class XmlCustomSerializer : CustomSerializer
    {
        private readonly XmlSerializer _bankClientsXmlSerializer;
        private readonly XmlSerializer _reportXmlSerializer;

        public XmlCustomSerializer()
        {
            _bankClientsXmlSerializer = new XmlSerializer(typeof(List<BankClient>),
                new XmlRootAttribute("ArrayOfBankClient"));
            _reportXmlSerializer = new XmlSerializer(typeof(Report));
        }
        
        public override List<BankClient> ListBankClientDeserialize(StreamReader reader)
        {
            return (List<BankClient>) _bankClientsXmlSerializer.Deserialize(reader);
        }

        public override void ReportSerialize(StreamWriter writer, Report report)
        {
            _reportXmlSerializer.Serialize(writer, report);
        }
    }
}