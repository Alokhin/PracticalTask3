﻿using System;
using System.Collections.Generic;
using System.Linq;
using PracticalTask3.Extensions;
using PracticalTask3.Models;

namespace PracticalTask3
{
    public static class ReportMaker
    {
        public static Report MakeReport(List<BankClient> clients)
        {
            // 1. Sum debit and credit operations per april
            var sum = clients
                .SelectMany(cl => cl.Operations)
                .Where(o => o.Date.Month == 4)
                .Sum(o => o.Amount);
            Console.WriteLine("Sum debit and credit operations per april for all clients: {0}.\n",
                sum);

            // 2. Clients who did not withdraw money in April
            var noWithdrawAprilClients = clients
                .Where(cl => !cl.Operations.Any(
                        o => o.Date.Month == 4 && o.OperationType == OperationType.Credit))
                .Select(cl => cl.GetClientInfo())
                .ToList();
            if (!noWithdrawAprilClients.Any())
            {
                Console.WriteLine("There are no clients who did not withdraw money in April.\n");
            }
            else
            {
                Console.WriteLine("Clients who did not withdraw money in April:\n[{0}]\n", 
                    string.Join("; ", noWithdrawAprilClients));
            }

            // 3. Client with the largest sum of debit operations
            var debitMaxCl = clients.Select(cl => 
                new
                {
                    ClientInfo = cl.GetClientInfo(),
                    DebitSum = cl.Operations
                        .Where(o => o.OperationType == OperationType.Debit)
                        .Sum(o => o.Amount) 
                })
                .MaxBy(cl => cl.DebitSum);
            Console.WriteLine("Client with the largest sum of debit operations:\n {0}.\n Sum = {1}\n",
                debitMaxCl.ClientInfo, debitMaxCl.DebitSum);
            
            // 4. Client with the largest sum of credit operations
            var creditMaxCl = clients.Select(cl => 
                new
                {
                    ClientInfo = cl.GetClientInfo(),
                    CreditSum = cl.Operations
                        .Where(o => o.OperationType == OperationType.Credit)
                        .Sum(o => o.Amount)
                })
                .MaxBy(cl => cl.CreditSum);
            Console.WriteLine("Client with the largest sum of credit operations:\n {0}.\n Sum = {1}\n",
                creditMaxCl.ClientInfo, creditMaxCl.CreditSum);
            
            // 5. Client with the largest balance on the account as of May 1 00:00
            var selectedDay = new DateTime(2018, 5, 1);
            var selectedDayBalanceMax = clients.Select(cl => 
                new
                {
                    ClientInfo = cl.GetClientInfo(),
                    Balance = cl.Operations
                            .Where(o => o.Date.CompareTo(selectedDay) < 0)
                            .Select(o => o.Amount * (o.OperationType == OperationType.Debit ? 1 : -1))
                            .Sum()
                })
                .MaxBy(cl => cl.Balance);
            Console.WriteLine("Client with the largest balance on the account as of May 1 00:00:\n {0}.\n Balance = {1}\n",
                selectedDayBalanceMax.ClientInfo, selectedDayBalanceMax.Balance);

            return new Report()
            {
                AprilSum = sum,
                NoWithdrawAprilClients = noWithdrawAprilClients,
                MaxDebitClient = debitMaxCl.ClientInfo,
                MaxCreditClient = creditMaxCl.ClientInfo,
                MaxMayBalanceClient = selectedDayBalanceMax.ClientInfo
            };
        }
    }
}