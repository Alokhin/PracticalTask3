﻿using System;
using System.Collections.Generic;

namespace PracticalTask3.Extensions
{
    public static class EnumerableExtension
    {
        /// <summary>
        /// Returns the maximal elements of the given sequence, based on
        /// the given projection.
        /// </summary>
        /// <param name="source">Source sequence</param>
        /// <param name="selector">Selector to use to pick the results to compare</param>
        /// <typeparam name="TSource">Type of the source sequence</typeparam>
        /// <typeparam name="TValue">Type of the projected element</typeparam>
        /// <returns>The maximal element, according to the projection.</returns>
        public static TSource MaxBy<TSource, TValue>(this IEnumerable<TSource> source,
            Func<TSource, TValue> selector) where TValue : IComparable<TValue>
        {
            var maxElement = default(TSource);
            foreach (var element in source)
            {
                if (maxElement == null || selector(element).CompareTo(selector(maxElement)) > 0)
                {
                    maxElement = element;
                }
            }
            return maxElement;
        }
    }
}