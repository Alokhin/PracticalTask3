﻿using System.Collections.Generic;
using System.Linq;

namespace PracticalTask3.Models
{
    public class BankClient
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public List<MoneyOperation> Operations { get; set; }    

        public BankClientInfo GetClientInfo()
        {
            return new BankClientInfo()
            {
                FirstName = FirstName,
                MiddleName = MiddleName,
                LastName = LastName,
                FirstDepositDate = Operations.FirstOrDefault(o => o.OperationType == OperationType.Debit)?.Date 
            };
        }  
    }
}