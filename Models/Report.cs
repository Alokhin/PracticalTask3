﻿using System.Collections.Generic;

namespace PracticalTask3.Models
{
    public class Report
    {
        public decimal AprilSum { get; set; }
        public List<BankClientInfo> NoWithdrawAprilClients { get; set; }
        public BankClientInfo MaxDebitClient { get; set; }
        public BankClientInfo MaxCreditClient { get; set; }
        public BankClientInfo MaxMayBalanceClient { get; set; }
    }
}