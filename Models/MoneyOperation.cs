﻿using System;
using System.Xml.Serialization;

namespace PracticalTask3.Models
{
    public enum OperationType
    {
        [XmlEnum("expense")]
        Credit,
        [XmlEnum("income")]
        Debit
    }
    
    public class MoneyOperation
    {
        public decimal Amount { get; set; }    
        public DateTime Date { get; set; }    
        [XmlAttribute("type")]
        public OperationType OperationType { get; set; }            
    }
}