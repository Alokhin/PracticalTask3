﻿using System;
using System.Xml.Serialization;

namespace PracticalTask3.Models
{
    public class BankClientInfo
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        
        [XmlElement(DataType = "date")]
        public DateTime? FirstDepositDate { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}, first deposit date {3}",
                FirstName, MiddleName, LastName,
                FirstDepositDate?.ToShortDateString() ?? "NONE");
        }
    }
}