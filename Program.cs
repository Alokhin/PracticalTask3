﻿using System;
using System.IO;
using PracticalTask3.Serializers;

namespace PracticalTask3
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            const string filesDir = "../../Files/";
            var bankClientsXmlDir = Path.Combine(filesDir, "bankClients.xml");
            var bankClientsJsonDir = Path.Combine(filesDir, "bankClients.json");
            var reportXmlDir = Path.Combine(filesDir, "report.xml");
            var reportJsonDir = Path.Combine(filesDir, "report.json");

            CustomSerializer serializer;
            string input, output;
            
            Console.WriteLine("Do you want to use xml? (type 'yes' or 'y' in another case you will use json)");
            
            var answer = Console.ReadLine();
            if (answer == "yes" || answer == "y")
            {
                serializer = new XmlCustomSerializer();    
                input = bankClientsXmlDir;
                output = reportXmlDir;
            }
            else
            {
                serializer = new JsonCustomSerializer();
                input = bankClientsJsonDir;
                output = reportJsonDir;
            }

            try
            {
                var clients = serializer.ListBankClientDeserialize(input);
                var report = ReportMaker.MakeReport(clients);
                serializer.ReportSerialize(output, report);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            Console.ReadKey();
        }
    }
}